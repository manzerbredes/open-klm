Welcome to open-klm !
===================

This project provide user interface to change msi Keyboard Light Color (like MSI Keyboard Light Manager).

----------


![klm.jpg](https://raw.githubusercontent.com/manzerbredes/open-klm/develop/resources/images/klm.jpg)


----------

How to use it ?
-------------

Simply download the jar from `'/lib/release/'`. Maybe you need to run the jar file **as root** user to make it work ! You can pass color arguments to set during your session startup :
> **Example :**  java -jar open-klm.jar --lc BLUE --mc RED --rc SKY --mode BREATHE <br />
> **Available color :**  OFF, RED, ORANGE, YELLOW, GREEN, SKY, BLUE, PURPLE, WHITE.<br />
> **Available mode :**  NORMAL, WAVE, BREATHE, GAMING, DEMO.


In development...
-------------
