package org.manzerbredes.open_klm.app;

import org.manzerbredes.open_klm.args.ArgsManager;
import org.manzerbredes.open_klm.client.MainWindow;
import org.manzerbredes.open_klm.drivers.*;



/**
 * Open KLM Application
 *
 */
public class App 
{
	
	/**
	 * Entry point
	 * 
	 * @param args
	 */
    public static void main( String[] args ) 
    {
    	// Get driver
    	DriverManager driverManager=new DriverManager();
    	Driver aDriver=driverManager.getDevice();
    	
    	// If a driver is found run the program
    	if(aDriver!=null){
    		
    		// Parse argument
    		ArgsManager argsManager=new ArgsManager();
    		argsManager.parse(aDriver, args);
    		
    		// Else run GUI
    		try {
				new MainWindow(aDriver);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    	else{
    		// Exit with error
        	System.err.println("No driver avalaible for your system. Try as root !");
        	System.exit(1);
    	}
    	
    	
    
    	
    }
}

