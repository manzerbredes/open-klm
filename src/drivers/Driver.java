package org.manzerbredes.open_klm.drivers;


/**
 * Driver must implement this interface
 * 
 * @author Manzerbredes
 *
 */
public interface Driver{
	
	/**
	 * Get the type of the driver
	 * 
	 * @return class that represent the type of the driver (DriverTypeA.class for example)
	 */
	public Class<?> getType();
	
	/**
	 * Initialize the driver (do not initialize anything in the constructor).
	 * 
	 * @return true if success (device is present and accessible) false else.
	 */
	public boolean initDriver();
}