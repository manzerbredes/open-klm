package org.manzerbredes.open_klm.drivers;

/**
 * Driver of type A (driver with is own functionalities).
 * Each driver must implement a Driver Type (to bind it's functionalities to
 * the user interface).
 * 
 * @author Manzerbredes
 *
 */
public interface DriverTypeA{
	
	
	/**
	 * Defined Region Helper
	 * 
	 * @author Manzerbredes
	 *
	 */
	public enum Region{
		LEFT(1), MIDDLE(2), RIGHT(3);
		
		private int current;
		
		Region(int current){
			this.current=current;
		}
		
		public int intValue(){
			return this.current;
		}
	}
	
	/**
	 * Defined Color Helper
	 * 
	 * @author Manzerbredes
	 *
	 */
	public enum Color{
		OFF(0),RED(1),ORANGE(2),YELLOW(3),GREEN(4),SKY(5), BLUE(6),PURPLE(7),WHITE(8);
		
		private int current;
		
		Color(int current){
			this.current=current;
		}
		
		public int intValue(){
			return this.current;
		}
	}

	/**
	 * Defined Level Helper
	 * 
	 * @author Manzerbredes
	 *
	 */
	public enum Intensity{
		HIGH(0), MEDIUM(1), LOW(2), LIGHT(3);
		
		private int current;
		
		Intensity(int current){
			this.current=current;
		}
		
		public int intValue(){
			return this.current;
		}
	}

	/**
	 * Defined Mode Helper
	 * 
	 * @author Manzerbredes
	 *
	 */
	public enum Mode{
		NORMAL(1), GAMING(2), BREATHE(3), DEMO(4), WAVE(5);
		
		private int current;
		
		Mode(int current){
			this.current=current;
		}
		
		public int intValue(){
			return this.current;
		}
	}


	/**
	 * Set color of the region
	 * @param region
	 * @param color
	 * @param intensity
	 */
	public void setRegionColor(Region region, Color color, Intensity intensity);
	
	/**
	 * Set global keyboard color
	 * @param color
	 * @param intensity
	 */
	public void setColor(Color color, Intensity intensity);
	
	/**
	 * Set global secondary color (for waves)
	 * @param color
	 * @param intensity
	 */
	public void setSecondaryColor(Color color, Intensity intensity);
	
	/**
	 * Set secondary color (for waves) by region
	 * @param region
	 * @param color
	 * @param intensity
	 */
	public void setSecondaryRegionColor(Region region, Color color, Intensity intensity);
	
	/**
	 * Set keyboard mode
	 * @param mode
	 */
	public void setMode(Mode mode);

}