package org.manzerbredes.open_klm.drivers;


/**
 * Driver Manager
 * 
 * @author Manzerbredes
 *
 */
public class DriverManager{
	
	/**
	 * List of available drivers
	 */
	private Class<?>[] drivers={
		Driver_1770_ff00.class
	};
	
	
	/**
	 * Get a successfully loaded driver
	 * 
	 * @return Driver the loaded driver.
	 */
	public Driver getDevice(){
		// Walk on driver list
		for(int i=0;i<this.drivers.length;i++){
			// Try to load each drivers
			try {
				Driver drv=(Driver) this.drivers[i].newInstance();
				// If success return it
				if(drv.initDriver())
					return drv;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		// If no driver available
		return null;
	}	
}