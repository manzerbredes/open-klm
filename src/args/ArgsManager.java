package org.manzerbredes.open_klm.args;

import org.manzerbredes.open_klm.drivers.Driver;


/**
 * 
 * Arguments manager
 * 
 * @author Manzerbredes
 *
 */
public class ArgsManager{
	
	/**
	 * List of Arguments Parser available
	 */
	private Class<?>[] parsers={
		ArgsTypeA.class
	};
	
	
	/**
	 * 
	 * Parse the arguments and exit
	 * 
	 * @param aDriver driver used to parse
	 * @param args arguments to parse
	 */
	public void parse(Driver aDriver, String[] args){
		if(args.length>0){
			for(Class<?> argsParser : this.parsers){
				ArgsParser parser;
				try {
					parser = (ArgsParser) argsParser.newInstance();
					if(parser.getType().equals(aDriver.getType())){
						parser.applyAndExit(aDriver, args);
					}
				} catch (Exception e){
					System.err.println(e.getMessage());
					System.exit(1);
				}
			}
		}
	}
	
}