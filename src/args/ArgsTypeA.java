package org.manzerbredes.open_klm.args;

import java.util.HashMap;

import org.javatuples.Pair;
import org.manzerbredes.open_klm.drivers.Driver;
import org.manzerbredes.open_klm.drivers.DriverTypeA;
import org.manzerbredes.open_klm.drivers.DriverTypeA.*;

import joptsimple.OptionParser;
import joptsimple.OptionSet;

/**
 * Parse args for DriverTypeA
 * 
 * @author Manzerbredes
 *
 */
public class ArgsTypeA implements ArgsParser{
	/**
	 * Define Keyboard primary color state
	 */
	private HashMap<Region, Pair<Color,Intensity>> primaryColorsState=new HashMap<>();
	
	/**
	 * Define Keyboard secondary color state (for wave)
	 */
	private HashMap<Region, Pair<Color,Intensity>> secondaryColorsState=new HashMap<>();
	
	/**
	 * Define Keyboard mode state
	 */
	private Mode mode=Mode.NORMAL;
	
	
	/**
	 * Build a ArgsTypeA
	 * 
	 * @param driver The driver to use
	 */
	public ArgsTypeA(){
		// Init primary color state
		this.primaryColorsState.put(Region.LEFT, new Pair<DriverTypeA.Color, DriverTypeA.Intensity>(Color.OFF, Intensity.HIGH));
		this.primaryColorsState.put(Region.MIDDLE, new Pair<DriverTypeA.Color, DriverTypeA.Intensity>(Color.OFF, Intensity.HIGH));
		this.primaryColorsState.put(Region.RIGHT, new Pair<DriverTypeA.Color, DriverTypeA.Intensity>(Color.OFF, Intensity.HIGH));
		// Init secondary color state
		this.secondaryColorsState.put(Region.LEFT, new Pair<DriverTypeA.Color, DriverTypeA.Intensity>(Color.OFF, Intensity.LOW));
		this.secondaryColorsState.put(Region.MIDDLE, new Pair<DriverTypeA.Color, DriverTypeA.Intensity>(Color.OFF, Intensity.LOW));
		this.secondaryColorsState.put(Region.RIGHT, new Pair<DriverTypeA.Color, DriverTypeA.Intensity>(Color.OFF, Intensity.LOW));
		// Init mode
		this.mode=Mode.NORMAL;
	}
	
	
	@Override
	public void applyAndExit(Driver aDriver, String[] args){
		if(aDriver.getType().equals(DriverTypeA.class)){
			// Build arguments parser
			OptionParser argsParser = new OptionParser();
			
			// Define left color, middle color, right color
			argsParser.accepts("lc").withRequiredArg();
			argsParser.accepts("mc").withRequiredArg();
			argsParser.accepts("rc").withRequiredArg();
			
			// Define left secondary color, middle secondary color, right secondary color
			argsParser.accepts("lsc").withRequiredArg();
			argsParser.accepts("msc").withRequiredArg();
			argsParser.accepts("rsc").withRequiredArg();
			
			// Define mode
			argsParser.accepts("mode").withRequiredArg();

			
			// Parse the options
			OptionSet parsedOptions = argsParser.parse(args);
			
			// Try to apply options
			try {
				if(parsedOptions.hasArgument("lc")){
					this.primaryColorsState.put(Region.LEFT, new Pair<DriverTypeA.Color, DriverTypeA.Intensity>(Color.valueOf((String) parsedOptions.valueOf("lc")),Intensity.HIGH));
				}
				if(parsedOptions.hasArgument("mc")){
					this.primaryColorsState.put(Region.MIDDLE, new Pair<DriverTypeA.Color, DriverTypeA.Intensity>(Color.valueOf((String) parsedOptions.valueOf("mc")),Intensity.HIGH));
				}
				if(parsedOptions.hasArgument("rc")){
					this.primaryColorsState.put(Region.RIGHT, new Pair<DriverTypeA.Color, DriverTypeA.Intensity>(Color.valueOf((String) parsedOptions.valueOf("rc")),Intensity.HIGH));
				}
				if(parsedOptions.hasArgument("lsc")){
					this.secondaryColorsState.put(Region.LEFT, new Pair<DriverTypeA.Color, DriverTypeA.Intensity>(Color.valueOf((String) parsedOptions.valueOf("lsc")),Intensity.HIGH));
				}
				if(parsedOptions.hasArgument("msc")){
					this.secondaryColorsState.put(Region.MIDDLE, new Pair<DriverTypeA.Color, DriverTypeA.Intensity>(Color.valueOf((String) parsedOptions.valueOf("msc")),Intensity.HIGH));
				}
				if(parsedOptions.hasArgument("rsc")){
					this.secondaryColorsState.put(Region.RIGHT, new Pair<DriverTypeA.Color, DriverTypeA.Intensity>(Color.valueOf((String) parsedOptions.valueOf("rsc")),Intensity.HIGH));
				}
				if(parsedOptions.hasArgument("mode")){
					this.mode=Mode.valueOf(parsedOptions.argumentOf("mode"));
				}
			}
			catch(Exception e){
				System.err.println(e.getMessage());
				System.exit(1);
			}
			
			// Apply argument
			this.applyArguments((DriverTypeA) aDriver);
			
			// Exit after apply
			System.exit(0);
		}
		
		// Error invalid driver
		System.exit(1);
	}
	
	
	
	/**
	 * Apply to device
	 */
	private void applyArguments(DriverTypeA driver){
		for(Region region:Region.values()){
			driver.setRegionColor(region, this.primaryColorsState.get(region).getValue0(), this.primaryColorsState.get(region).getValue1());
		}
		for(Region region:Region.values()){
			driver.setSecondaryRegionColor(region, this.secondaryColorsState.get(region).getValue0(), this.primaryColorsState.get(region).getValue1());
		}
		driver.setMode(this.mode);
	}


	@Override
	public Class<?> getType() {
		return DriverTypeA.class;
	}
	
}