package org.manzerbredes.open_klm.args;

import org.manzerbredes.open_klm.drivers.Driver;

/**
 * All arguments parser must implement this interface
 * 
 * @author Manzerbredes
 *
 */
public interface ArgsParser{
	/**
	 * Apply parsed argument and exit the program
	 */
	public void applyAndExit(Driver aDriver, String[] args);
	
	/**
	 * Get the type of the driver the parser should parse
	 * 
	 * @return class that represent the type of the driver (DriverTypeA.class for example)
	 */
	public Class<?> getType();
}