package org.manzerbredes.open_klm.client;


import javax.swing.*;
import org.manzerbredes.open_klm.drivers.*;


/**
 * Main Window
 * 
 * @author Manzerbredes
 *
 */
public class MainWindow extends JFrame {
	

	/**
	 * Define serial Version UID
	 */
	private static final long serialVersionUID = 8058826286308946977L;
	
	
	/**
	 * Contain all JPanel corresponding to each driver type
	 */
	private Class<?>[] driverJPanels={
			JPanelTypeA.class
	};
	
	
	/**
	 * Build a main window
	 * @param aDriver
	 */
	public MainWindow(Driver aDriver){
		// Configure main window
		this.initUI();
	
        // Add driver panel
        for(int i=0;i<this.driverJPanels.length;i++){
        	try {
        		// Build a panel
				DriverJPanel driverJPanel=(DriverJPanel) driverJPanels[i].newInstance();
				// If the panel have the same type of the driver try to init it
				if(driverJPanel.getType().equals(aDriver.getType())){
					// If init success add it to the main window
					if(driverJPanel.initUI(aDriver)){
						this.add((JPanel) driverJPanel);
					}
				}
				
			} catch (Exception e) {
				e.printStackTrace();
			}
        }
        
        // Display the main window
		this.setVisible(true);
	}
	
	
	/**
	 * Configure main window
	 */
	private void initUI(){
		// Configure MainWindow
		this.setTitle("Open KLM");
		this.setSize(700, 500);
        setLocationRelativeTo(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
	}

	
}