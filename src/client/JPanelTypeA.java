package org.manzerbredes.open_klm.client;

import java.awt.Label;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JPanel;

import org.manzerbredes.open_klm.drivers.Driver;
import org.manzerbredes.open_klm.drivers.DriverTypeA;
import org.manzerbredes.open_klm.drivers.DriverTypeA.Color;
import org.manzerbredes.open_klm.drivers.DriverTypeA.Intensity;
import org.manzerbredes.open_klm.drivers.DriverTypeA.Region;

public class JPanelTypeA extends JPanel implements DriverJPanel{
	
	
	private JComboBox<Color> left;
	private JComboBox<Color> middle;
	private JComboBox<Color> right;
	private JButton apply=new JButton("Apply");
	private DriverTypeA keyboardTypeA;

	@Override
	public boolean initUI(Driver driver){
		
		if(driver.getType().equals(DriverTypeA.class)){
			
			this.keyboardTypeA=(DriverTypeA) driver;
			
			this.left=new JComboBox<>(Color.values());
			this.middle=new JComboBox<>(Color.values());
			this.right=new JComboBox<>(Color.values());
			
			this.apply.addActionListener(new ActionListener() {
				
				@Override
				public void actionPerformed(ActionEvent e) {
					Color leftRegion=(Color) left.getSelectedItem();
					Color middleRegion=(Color) middle.getSelectedItem();
					Color rightRegion=(Color) right.getSelectedItem();
					
					keyboardTypeA.setRegionColor(Region.LEFT, leftRegion, Intensity.HIGH);
					keyboardTypeA.setRegionColor(Region.MIDDLE, middleRegion, Intensity.HIGH);
					keyboardTypeA.setRegionColor(Region.RIGHT, rightRegion, Intensity.HIGH);
	
					
				}
			});
			BoxLayout gridLayout=new BoxLayout(this, BoxLayout.Y_AXIS);
			this.add(new Label("Test interface"));
			this.add(left);
			this.add(middle);
			this.add(right);
			this.add(this.apply);
			return true;
		}
		return false;
	}

	@Override
	public Class<?> getType() {
		return DriverTypeA.class;
	}
	
}