package org.manzerbredes.open_klm.client;

import org.manzerbredes.open_klm.drivers.Driver;

/**
 * JPanel for a specific driver. Each panel who manage
 * a driver must implement this interface.
 * 
 * @author Manzerbredes
 *
 */
public interface DriverJPanel{
	
	
	/**
	 * Get the type of driver handled by the JPanel
	 * 
	 * @return class Class that represent the type of the driver the JPanel handle
	 */
	public Class<?> getType();
	
	/**
	 * 
	 * Init the JPanel with a driver (driver must have the correct type)
	 * 
	 * @param driver The driver to manage
	 * @return True if success to init, False else
	 */
	public boolean initUI(Driver driver);
}